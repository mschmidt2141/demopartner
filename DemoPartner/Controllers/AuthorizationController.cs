using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using JWT;
using JWT.Algorithms;
using JWT.Serializers;
using Newtonsoft.Json;

using DemoPartner.Models;

namespace DemoPartner.Controllers
{
    [Produces("application/json")]
    [Route("api/Authorization")]
    public class AuthorizationController : Controller
    {
        private const int TimeToLiveInMinutes = 5;
        private const string TadsCliendId = "cH#Q2t5stap=";
        private const string TadsClientSecret = "d6swET8ApHuh7sur4K6Su5eS6@Vephes";

        private static HashSet<string> IssuedCodes = new HashSet<string>();
        private static HashSet<string> UsedCodes = new HashSet<string>();
        private static HashSet<User> Users;

        // Static Constructor
        static AuthorizationController()
        {
            Users = new HashSet<User>
            {
                new User
                {
                    Birthdate = new DateTime(1973, 4, 1),
                    Email = "matts@tads.com",
                    FirstName = "Matthew",
                    LastName = "Schmidt",
                    UserId = 123456789
                }
            };
        }

        // Public Methods
        [HttpGet]
        [Route("")]
        public IActionResult Authorize(
            [Bind(Prefix = "response_type")] string responseType, [Bind(Prefix = "client_id")] string clientId, [Bind(Prefix = "redirect_uri")] string redirectUri, string scope, string state)
        {
            IActionResult result = null;

            if (responseType != "code")
            {
                result = BadRequest("Parameter response_type must be 'code'");
            }
            else if (!scope.Contains("openid"))
            {
                result = BadRequest("Paramter scope must contain 'openid'");
            }
            else
            {
                var code = GenerateRandomNumber(16);
                var dateTimeProvider = new UtcDateTimeProvider();
                var secret = TadsClientSecret;
                var user = Users.First();

                IssuedCodes.Add(code);

                var now = dateTimeProvider.GetNow();
                var payload = new Dictionary<string, object>()
                {
                    { "aud", TadsCliendId },
                    { "exp", (int)(now.AddMinutes(TimeToLiveInMinutes) - JwtValidator.UnixEpoch).TotalSeconds },
                    { "iat", (int)(now - JwtValidator.UnixEpoch).TotalSeconds },
                    { "iss", "https://demopartner.example.com" },
                    { "nonce", GenerateRandomNumber(50) },
                    { "sub", user.UserId },
                    { "birthdate", user.Birthdate.ToShortDateString() },
                    { "email", user.Email },
                    { "given_name", user.FirstName },
                    { "family_name", user.LastName }
                };

                IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
                IJsonSerializer serializer = new JsonNetSerializer();
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

                var token = encoder.Encode(payload, secret);

                result = Redirect($"{redirectUri}?code={code}&id_token={token}&state={state}&scope={scope}&state={state}");
            }

            return result;
        }

        [HttpPost]
        [Route("Token")]
        public IActionResult Token([Bind(Prefix = "grant_type")] string grantType, string code, [Bind(Prefix = "redirect_uri")] string redirectUri)
        {
            IActionResult result = null;

            if (grantType != "authorization_token")
            {
                result = BadRequest("The parameter grant_type must be 'authorization_token'");
            }
            else if (!IssuedCodes.Contains(code))
            {
                result = BadRequest("The provided code is invalid");
            }
            else if (UsedCodes.Contains(code))
            {
                result = BadRequest("The code provided has already been used");
            }

            var dateTimeProvider = new UtcDateTimeProvider();
            var secret = TadsClientSecret;
            var user = Users.First();

            IssuedCodes.Add(code);

            var now = dateTimeProvider.GetNow();
            var payload = new Dictionary<string, object>()
            {
                { "aud", TadsCliendId },
                { "exp", (int)(now.AddMinutes(TimeToLiveInMinutes) - JwtValidator.UnixEpoch).TotalSeconds },
                { "iat", (int)(now - JwtValidator.UnixEpoch).TotalSeconds },
                { "iss", "https://demopartner.example.com" },
                { "sub", user.UserId },
                { "birthdate", user.Birthdate.ToShortDateString() },
                { "email", user.Email },
                { "given_name", user.FirstName },
                { "family_name", user.LastName }
            };

            IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
            IJsonSerializer serializer = new JsonNetSerializer();
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtEncoder encoder = new JwtEncoder(algorithm, serializer, urlEncoder);

            var token = new TokenResult
            {
                AccessToken = GenerateRandomNumber(16),
                Expires = 3600,
                RefreshToken = GenerateRandomNumber(16),
                Token = encoder.Encode(payload, secret),
                TokenType = "Bearer"
            };

            result = Content(JsonConvert.SerializeObject(token));

            return result;
        }

        // Private Methods
        private string GenerateRandomNumber(int length)
        {
            var data = new byte[length];
            var generator = RandomNumberGenerator.Create();

            generator.GetBytes(data);

            return Convert.ToBase64String(data);
        }
    }
}