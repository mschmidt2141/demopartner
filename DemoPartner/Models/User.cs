﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DemoPartner.Models
{
    public class User
    {
        // Public Constructor
        public User() { }

        // Public Properties
        public DateTime Birthdate { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int UserId { get; set; }
    }
}
